# Docstring:
"""
ASC Noise Budget Guardian

This guardian simply uses the ASC-NOISE bank to send white noise from an
always running 

Authors: A. Mullavey, M. Kasprzack
Date: 2020-11-10
Contact: adam.mullavey@ligo.org

Updated by: A. Effler, S. Nichols
Date: 2022-05-25
"""

#------------------------------------------------------------------------------
# Imports
from guardian import GuardState, GuardStateDecorator
import isclib.matrices as matrix
import time
import numpy as np
import gpstime
import csv
from os import path

#------------------------------------------------------------------------------
 
nominal = "IDLE"
request = nominal

#------------------------------------------------------------------------------
# Variables

fieldnames = ["Bkgrnd Start","Bkgrnd Stop","Inj Start","Inj Stop"]

# top path TODO: decide on directory structure
pathname = "/data/l1nb/meas_times/"

bkgrnd_fname = "background_times"


out_matrix = {
    "DHARD_P": np.matrix([[-0.87,0.87],[1.0,-1.0]]),
    "DHARD_Y": np.matrix([[0.87,0.87],[1.0,1.0]]),
    "CHARD_P": np.matrix([[-0.87,-0.87],[1.0,1.0]]),
    "CHARD_Y": np.matrix([[0.87,-0.87],[1.0,-1.0]]),
    "DSOFT_P": np.matrix([[0,-1.0],[0,-0.87]]),
    "DSOFT_Y": np.matrix([[0,1.0],[0,-0.87]]),
    "CSOFT_P": np.matrix([[1.0,0],[0.87,0]]),
    "CSOFT_Y": np.matrix([[-1.0,0],[0.87,0]])
}


#------------------------------------------------------------------------------
# Functions


def set_fbank_filts(dof,filts=[]):
    ezca.write("ASC-NOISE_"+dof.upper()+"_TRAMP",0)
    time.sleep(0.1)

    ezca.write("ASC-NOISE_"+dof.upper()+"_GAIN",0)

    ezca.switch("ASC-NOISE_"+dof.upper(),"FMALL","OFF")
    time.sleep(0.1)

    ezca.switch("ASC-NOISE_"+dof.upper(),"INPUT", "OUTPUT","ON")
    time.sleep(0.1)


    if len(filts) > 0:
        for filt in filts:
            ezca.switch("ASC-NOISE_"+dof.upper(),"FM{}".format(filt),"ON")


def set_fbank_gain(dof,gain=1.0,tramp=0.1,sleep=0):
    ezca.write("ASC-NOISE_"+dof.upper()+"_TRAMP",tramp)
    time.sleep(0.3)
    ezca.write("ASC-NOISE_"+dof.upper()+"_GAIN",gain)
    time.sleep(sleep)


def reset_fbank(dof):
    set_fbank_gain(dof,gain=0.0,tramp=1.0,sleep=1.0)
    set_fbank_filts(dof,filts=[])



#------------------------------------------------------------------------------
# Guardian Decorators



#------------------------------------------------------------------------------
# State Generators

# MICH, PRCL and SRCL states are very similar, hence this common state generator
def make_inj_state(dof1,dof2,gain=1.0,filts=[]):

    if dof2.upper() == "P":
        asc_mtrx = matrix.asc_output_pit
    elif dof2.upper() == "Y":
        asc_mtrx = matrix.asc_output_yaw

    if dof1 != "MICH":
        mtrx_vals = out_matrix[dof1+"_"+dof2]

    class INJ_FOR_DOF(GuardState):
        request = True

        def main(self):

            #self.dof = dof1

            # Self times 
            #self.meas_times = {}

            ### Check background
            if not path.exists(pathname + bkgrnd_fname + ".csv"):
                log("Background file doesn't exist.")
                return "TAKE_BACKGROUND"
        
            with open(pathname + bkgrnd_fname + ".csv","r") as csvfile:
                reader = csv.DictReader(csvfile)
                list = []
                for row in reader:
                    list.append(dict(row))
                self.meas_times = list[-1]

            # If backgound is older than 60 minutes (?), take it again. TODO: How old can the background be?
            if gpstime.gpsnow()-float(self.meas_times["Bkgrnd Stop"]) > 3600:
                log("Last background too old, taking new background.")
                return "TAKE_BACKGROUND"
            ###

            # File details, TODO: set directory and filename
            filename ="{0}_{1}_inj_times".format(dof1.lower(),dof2.lower())
            self.filename = pathname + filename + ".csv"

            # If file doesn't exist, create it.
            if not path.exists(self.filename):
                with open(self.filename,"w+") as csvfile:
                    writer = csv.DictWriter(csvfile,fieldnames=fieldnames)
                    writer.writeheader()

            # Set the filter bank, set gain to zero, TODO: decide on filter settings
            set_fbank_gain(dof2,0,tramp=0.1,sleep=0.1)
            set_fbank_filts(dof2,filts)

            # Time of measurement and ramp, TODO: set measurement time
            self.t_ramp = 3.0
            self.t_measurement = 64.0

            # Some flags
            self.record_start = True
            self.record_stop = True
            self.ramp_down = True
            self.write_to_csv = True


            # Set the matrix
            if dof1 == 'MICH':
                asc_mtrx['BS','NOISE'] = 1
            else:
                # TODO, need to set the basis
                #log("matrix is {}".format(mtrx_vals))
                log("matrix is {}".format(out_matrix[dof1+"_"+dof2]))
                log("matrix 00 is {}".format(mtrx_vals[0,0]))
                asc_mtrx["ITMX","NOISE"] = mtrx_vals[0,0]
                asc_mtrx["ITMY","NOISE"] = mtrx_vals[0,1]
                asc_mtrx["ETMX","NOISE"] = mtrx_vals[1,0]
                asc_mtrx["ETMY","NOISE"] = mtrx_vals[1,1]
                time.sleep(0.1)

            # set ADS gain to zero
            ADS_Gain = ezca['ASC-ADS_GAIN']
            #ezca['ASC-ADS_GAIN'] = 0 

            # Ramp up the gain, effectively starting the injection
            set_fbank_gain(dof2,gain,tramp=self.t_ramp,sleep=self.t_ramp)


        def run(self):

            # Wait for matrix to stop ramping
            #FIXME: check if filter is ramping up?
            #if asc_mtrx.is_ramping(dof1,"NOISE"):
            #    return

            # Store the start time
            if self.record_start:
                self.timer["meas"] = self.t_measurement
                self.meas_times["Inj Start"] = gpstime.gpsnow()
                self.record_start = False
                

            # Keep returning until injection time is up
            if not self.timer["meas"]:
                notify("{0}_{1} Injection Running".format(dof1,dof2))
                return

            # Store the stop time
            if self.record_stop:
                log("Stopping injection, recording stop time")
                self.meas_times["Inj Stop"] = gpstime.gpsnow()
                self.record_stop = False

            # Ramp down the injection
            if self.ramp_down:
                log("Ramping down gain")
                set_fbank_gain(dof2,0,tramp=self.t_ramp,sleep=self.t_ramp)
                log("Setting matrix elements to zero")
                for act in ["ITMX","ITMY","ETMX","ETMY","BS"]:
                    asc_mtrx[act,'NOISE'] = 0
                time.sleep(0.1)
                self.ramp_down = False

            #reset ADS gain
            #ezca['ASC-ADS_GAIN'] = ADS_Gain

            # Write everything to a csv file
            if self.write_to_csv:
                with open(self.filename,"a") as csvfile:
                    writer = csv.DictWriter(csvfile,fieldnames=fieldnames)
                    writer.writerow(self.meas_times)
                self.write_to_csv = False

            notify("{0}_{1} Injection Complete".format(dof1,dof2))

            return True

    return INJ_FOR_DOF

#------------------------------------------------------------------------------
#                                 Guardian States
#------------------------------------------------------------------------------

class IDLE(GuardState):
    #goto = True
    request = True
    index = 5
    
    # Main class method
    def main(self):


        log("Matrix is {}".format(out_matrix))

        # Set a timer
        self.timer["wait"] = 2

    # Run class method.
    def run(self):
        # Check for timer expiration.
        if self.timer["wait"]:
            # Timer has expired so proceed.
            return True



class RESET_NOISE_INJ(GuardState):
    goto = True
    request = False
    index = 1

    def main(self):

        reset_fbank("P")
        reset_fbank("Y")
        
        matrix.asc_output_pit.zero(col='NOISE')
        matrix.asc_output_yaw.zero(col='NOISE')
        self.timer["wait"] = 1

    def run(self):

        if not self.timer["wait"]:
            return

        return True


class TAKE_BACKGROUND(GuardState):
    request = False
    index = 10

    def main(self):
        # File details
        self.filename = pathname + bkgrnd_fname + ".csv"

        # If file doesn't exist, create it.
        if not path.exists(self.filename):
            with open(self.filename,"w+") as csvfile:
                writer = csv.DictWriter(csvfile,fieldnames=fieldnames[0:2])
                writer.writeheader()

        # Self times 
        self.meas_times = {}


        # Time of measurement and ramp, TODO: set measurement time
        self.t_measurement = 64.0

        # Flags
        self.record_start = True
        self.record_stop = True
        self.write_to_csv = True

    def run(self):

        # Store the start time
        if self.record_start:
            self.timer["meas"] = self.t_measurement
            self.meas_times["Bkgrnd Start"] = gpstime.gpsnow()
            self.record_start = False

        if not self.timer["meas"]:
            notify("Taking background data.")
            return

        if self.record_stop:
            log("Finished taking background data, recording stop time")
            self.meas_times["Bkgrnd Stop"] = gpstime.gpsnow()
            self.record_stop = False

        if self.write_to_csv:
            with open(self.filename,"a") as csvfile:
                writer = csv.DictWriter(csvfile,fieldnames=fieldnames[0:2])
                writer.writerow(self.meas_times)
            self.write_to_csv = False

        return True



INJ_FOR_DHARD_P = make_inj_state("DHARD","P",gain=0.12,filts=[1,3,4])
INJ_FOR_DHARD_P.index = 20

INJ_FOR_DHARD_Y = make_inj_state("DHARD","Y",gain=0.2,filts=[1,3,4])
INJ_FOR_DHARD_Y.index = 30

INJ_FOR_CHARD_P = make_inj_state("CHARD","P", gain=0.5, filts=[1,3,4])
INJ_FOR_CHARD_P.index = 40

INJ_FOR_CHARD_Y = make_inj_state("CHARD","Y", gain=0.45, filts =[1,3,4])
INJ_FOR_CHARD_Y.index = 50

INJ_FOR_DSOFT_P = make_inj_state("DSOFT","P",gain=0.45,filts=[2,3,4])
INJ_FOR_DSOFT_P.index = 60

INJ_FOR_DSOFT_Y = make_inj_state("DSOFT","Y",gain=0.9,filts=[2,3,4])
INJ_FOR_DSOFT_Y.index = 70

INJ_FOR_CSOFT_P = make_inj_state("CSOFT","P", gain=1.0, filts=[2,3,4])
INJ_FOR_CSOFT_P.index = 80

INJ_FOR_CSOFT_Y = make_inj_state("CSOFT","Y", gain=0.45, filts =[2,3,4])
INJ_FOR_CSOFT_Y.index = 90

INJ_FOR_MICH_P = make_inj_state("MICH","P", gain=40, filts=[6])
INJ_FOR_MICH_P.index = 100

INJ_FOR_MICH_Y = make_inj_state("MICH","Y", gain=40, filts =[6])
INJ_FOR_MICH_Y.index = 110



edges = [
    ("INIT", "IDLE"),
    ("RESET_NOISE_INJ","IDLE"),
    ("IDLE", "INJ_FOR_DHARD_P"),
    ("TAKE_BACKGROUND","INJ_FOR_DHARD_P"),
    ("IDLE", "INJ_FOR_DHARD_Y"),
    ("TAKE_BACKGROUND","INJ_FOR_DHARD_Y"),
    ("IDLE", "INJ_FOR_CHARD_P"),
    ("TAKE_BACKGROUND","INJ_FOR_CHARD_P"),
    ("IDLE", "INJ_FOR_CHARD_Y"),
    ("TAKE_BACKGROUND","INJ_FOR_CHARD_Y"),
    ("IDLE", "INJ_FOR_DSOFT_P"),
    ("TAKE_BACKGROUND","INJ_FOR_DSOFT_P"),
    ("IDLE", "INJ_FOR_DSOFT_Y"),
    ("TAKE_BACKGROUND","INJ_FOR_DSOFT_Y"),
    ("IDLE", "INJ_FOR_CSOFT_P"),
    ("TAKE_BACKGROUND","INJ_FOR_CSOFT_P"),
    ("IDLE", "INJ_FOR_CSOFT_Y"),
    ("TAKE_BACKGROUND","INJ_FOR_CSOFT_Y"),
    ("IDLE", "INJ_FOR_MICH_P"),
    ("TAKE_BACKGROUND","INJ_FOR_MICH_P"),
    ("IDLE", "INJ_FOR_MICH_Y"),
    ("TAKE_BACKGROUND","INJ_FOR_MICH_Y")
] 


    
